# phage_detection - a pipeline to identify complete phage genomes from assembled contigs

# Descriptions
phage_detection is a custom pipeline to identify complete (circular) phage genomes from assembled contigs used in the study of Nishijima et al (in revision).   
This pipeline classify contigs into phage or others based on various information obtained from each step of the workflow.

# Workflow of the phage_detection pipeline
step1. Extract circular contigs with >10kb in length

step2. Predict genes with MetaGeneMark

step3. Check the circular contigs with DeepVirFinder

step4. Identify viral and plasmid hallmark genes using HMM-HMM alignment with pipeline_for_high_sensitive_domain_search

step5. Identify bacterial marker genes using fetchMG and barrnap

step6. Count putative viral and plasmid genes by aligning to reference databases

step7. Check viral genome quality (e.g. completeness and contamination) by checkV

step8. Summarize the results and classify the contigs into phage/others

# Installation
git clone https://gitlab.com/suguru.nishijima/phage_detection/  
export PATH=$PATH:/path_on_your_system/phage_detection/scripts/


# Dependencies  

Please download and install the following tools to use phage_detection.  

MetaGeneMark (v3.38):
http://exon.gatech.edu/meta_gmhmmp.cgi

DeepVirFinder (v1.0):
https://github.com/jessieren/DeepVirFinder

HHsuite (v3.2.0):
https://github.com/soedinglab/hh-suite

fetchMG (v1.0): 
http://vm-lux.embl.de/~mende/fetchMG/about.html

Barrnap (v0.9): 
https://github.com/tseemann/barrnap

DIAMOND (v0.9.29.130): 
https://github.com/bbuchfink/diamond

pipeline_for_high_sensitive_domain_search (v0.1.3): 
https://github.com/yosuken/pipeline_for_high_sensitive_domain_search

CheckV (v0.7):
https://bitbucket.org/berkeleylab/checkv/src/master/


# Download databases
cd db/  
wget http://wwwuser.gwdg.de/~compbiol/data/hhsuite/databases/hhsuite_dbs/pfamA_32.0.tar.gz  
tar -zxvf pfamA_32.0.tar.gz  
checkv download_database ./

# PATH
export PATH=$PATH:/path_on_your_system/pipeline_for_high_sensitive_domain_search  
export PATH=$PATH:/path_on_your_system/MetaGeneMark_linux_64/mgm/  

export MGM_PARAM=/path_on_your_system/MetaGeneMark_linux_64/mgm/MetaGeneMark_v1.mod  
export CHECKVDB=db/checkv-db-v1.2/


# Usage
phage_detection \<input fasta\> \<output dir\>

# Example usage
phage_detection example_contigs.fasta out

# Output
`summary.tsv`: Summary file of the pipeline. 
1. Contig                   - circular contig detected in the assembled contigs
2. Classification                       - phage / others
3. Length                               - contig length
4. DeepVifFinder p-value                - p-value reported by DeepVirFinder
5. \# of phage hallmark genes            - number of phage hallmark genes
6. \# of plasmid hallmark genes          - number of plasmid hallmark genes
7. CheckV provirus                      - presence / absence of provirus region assessed by checkV
8. CheckV provirus proportion           - proportion of provirus revions in the contig
9. CheckV quality                       - quality defined by checkV
10. CheckV completeness                 - completeness reported by checkV
11. CheckV contamination                - contamination reported by checkV
12. Phage hit genes / plasmid hit genes - ratio of putative phage and plasmid genes 
13. Phage hit genes                     - number of proteins aligned to those in IMG/VR database
14. Plasmid hit genes                   - number of proteins aligned to those of plasmids in the RefSeq database
15. Bacterial marker genes              - number of bacterial marker genes identified by featchMG
16. 23S rRNA                            - number of 23S rRNA genes identified by barrnap
17. 16S rRNA                            - number of 16S rRNA genes identified by barrnap
18. 5S rRNA                             - number of 5S rRNA genes identified by barrnap

`phage_sequences.fna`: Complete phage sequences detected in the assembled contigs.

# Citation
`The main paper which used this pipeline`  
- phage_detection  
**Extensive gut virome variation and its associations with host and environmental factors in a population-level cohort**  
Suguru Nishijima*, Naoyoshi Nagata*, Yuya Kiguchi, Yasushi Kojima, Tohru Miyoshi-Akiyama, Moto Kimura, Mitsuru Ohsugi, Kohjiro Ueki, Shinichi Oka, Masashi Mizokami, Takao Itoi, Takashi Kawai, Naomi Uemura, Masahira Hattori  
In revision

`The papers for underlying software and algorithm used in each step of the phage_detection`  

- MetaGeneMark (v3.38):  
**Ab initio gene identification in metagenomic sequences**  
Wenhan Zhu, Alexandre Lomsadze, Mark Borodovsky  
Nucleic Acids Res. 2010 Jul;38(12):e132. doi: 10.1093/nar/gkq275. Epub 2010 Apr 19.

- DeepVirFinder (v1.0):  
**Identifying viruses from metagenomic data using deep learning**  
Jie Ren, Kai Song, Chao Deng, Nathan A Ahlgren, Jed A Fuhrman, Yi Li, Xiaohui Xie, Ryan Poplin, Fengzhu Sun  
Quant Biol. 2020 Mar;8(1):64-77. doi: 10.1007/s40484-019-0187-4.  

- HHsuite (v3.2.0):  
**HH-suite3 for fast remote homology detection and deep protein annotation**  
Martin Steinegger, Markus Meier, Milot Mirdita, Harald Vöhringer, Stephan J Haunsberger, Johannes Söding  
BMC Bioinformatics. 2019 Sep 14;20(1):473. doi: 10.1186/s12859-019-3019-7.  

- DIAMOND (v0.9.29.130):  
**Fast and sensitive protein alignment using DIAMOND**  
Benjamin Buchfink, Chao Xie, Daniel H Huson  
Nat Methods. 2015 Jan;12(1):59-60. doi: 10.1038/nmeth.3176. Epub 2014 Nov 17.  

- IMG/VR (v2.0):  
**IMG/VR v.2.0: an integrated data management and analysis system for cultivated and environmental viral genomes**  
David Paez-Espino, Simon Roux, I-Min A Chen, Krishna Palaniappan, Anna Ratner, Ken Chu, Marcel Huntemann, T B K Reddy, Joan Carles Pons, Mercè Llabrés, Emiley A Eloe-Fadrosh, Natalia N Ivanova, Nikos C Kyrpides  
Nucleic Acids Res. 2019 Jan 8;47(D1):D678-D686. doi: 10.1093/nar/gky1127.  

- pipeline_for_high_sensitive_domain_search (v0.1.3):  
**Environmental Viral Genomes Shed New Light on Virus-Host Interactions in the Ocean**  
Yosuke Nishimura, Hiroyasu Watai, Takashi Honda, Tomoko Mihara, Kimiho Omae, Simon Roux, Romain Blanc-Mathieu, Keigo Yamamoto, Pascal Hingamp, Yoshihiko Sako, Matthew B Sullivan, Susumu Goto, Hiroyuki Ogata, Takashi Yoshida  
mSphere. 2017 Mar 1;2(2):e00359-16. doi: 10.1128/mSphere.00359-16.  

- CheckV (v0.7):  
**CheckV assesses the quality and completeness of metagenome-assembled viral genomes**  
Stephen Nayfach, Antonio Pedro Camargo, Frederik Schulz, Emiley Eloe-Fadrosh, Simon Roux, Nikos C Kyrpides  
Nat Biotechnol. 2021 May;39(5):578-585. doi: 10.1038/s41587-020-00774-7.  
