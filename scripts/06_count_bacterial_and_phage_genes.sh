#!/bin/bash

i=$1
out=$2

mkdir $out/06

## similality search against plasmid proteins
plasmid_db=db/refseq_plasmid_genes.faa
diamond blastp -p 12 --more-sensitive -d $plasmid_db -q $out/02/$i.10k.fna.circ.fna.mgm.faa -o $out/06/$i.10k.fna.circ.fna.mgm.faa.plasmid.dmd

## similality search against IMG proteins
img_db=db/IMGVR_circular_phage_genes.faa
diamond blastp -p 12 --more-sensitive -d $img_db -q $out/02/$i.10k.fna.circ.fna.mgm.faa -o $out/06/$i.10k.fna.circ.fna.mgm.faa.img.dmd
