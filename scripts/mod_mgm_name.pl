#!/usr/bin/perl

$file=$ARGV[0];
$name=(split/\./,$file)[0];

open(file,"$file");
while(<file>){
    chomp;
    if(/>/){
	$contig=(split/\t/)[1];
	$contig=~s/ .*//;
	$num{$contig}++;
	$num=$num{$contig};
	if($contig =~ /\|/){
	    $contig = (split/\|/, $contig)[3];
	    $contig = ">$contig";
	}
	print"${contig}_${num}\n";
    }else{
	print"$_\n";
    }
}
