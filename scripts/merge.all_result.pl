#!/usr/bin/perl

$i = $ARGV[0];
$out = $ARGV[1];

## DeepVirFinder
open(file, "$out/03/$i.10k.fna.circ.fna.dvf/$i.10k.fna.circ.fna_gt1bp_dvfpred.txt");
while(<file>){
    chomp;
    ($contig, $length, $p) = (split/\t/)[0,1,3];
    $contig =~ s/ .*//;
    if($contig =~/^name/){next}
    $length{$contig} = $length;
    $dvf_p{$contig} = $p;
    $contig{$contig} = 1;
}

## checkV
open(file, "$out/07/$i.10k.fna.circ.fna.checkv/quality_summary.tsv");
while(<file>){
    chomp;
    $a++;
    if($a == 1){next}
    ($genome, $leng, $pro, $pro_leng, $checkvq, $comp, $conta) = (split/\t/)[0, 1, 2, 3, 7, 9, 11];
    #print "$genome\n";
    $checkv_pro{$genome} = $pro;
    $checkv_pro_pro{$genome} = ($pro_leng / $leng);
    $checkv_q{$genome} = $checkvq;
    $checkv_comp{$genome} = $comp;
    $checkv_conta{$genome} = $conta;
}

## rRNA
open(file, "$out/05/$i.10k.fna.circ.fna.rrna");
while(<file>){
    chomp;
    $contig = (split/\t/) [0];
    if(/23S ribosomal RNA$/){
	$rrna3{$contig}++;
    }elsif(/16S ribosomal RNA$/){
	$rrna2{$contig}++;
    }elsif(/5S ribosomal RNA$/){
	$rrna1{$contig}++;
    }
}

## marker genes
open(file, "$out/05/$i.10k.fna.circ.fna.mgm.faa.mg.faa");
while(<file>){
    chomp;
    if(/>/){
	$contig = $_;
	$contig =~s/>//;
	$contig =~s/_\d+?$//;
	$mg{$contig}++;
    }
}

## hhblits
open(file, "$out/04/$i.10k.fna.circ.fna.mgm.faa.hhblits.sort");
while(<file>){
    chomp;
    ($contig, $phage_pfam, $plasmid_pfam) = (split/\t/)[0,1,2];
    $phage_pfam{$contig} = $phage_pfam;
    $plasmid_pfam{$contig} = $plasmid_pfam;
}

## BLAST to plasmids
open(file, "$out/06/$i.10k.fna.circ.fna.mgm.faa.plasmid.dmd");
while(<file>){
    chomp;
    ($gene, $e) = (split/\t/)[0, 10];
    if($e>0.00001){next}
    $plasmid_hit{$gene} = 1;

    ($gene, $e) = (split/\t/)[0, 10];
    if($e>0.00001){next}
    $genome = $gene;
    $genome =~s /\_[0-9]{1,3}$//;
    if($plasmid_hit_gene{$genome} =~ /$gene/){next}
    $plasmid_hit_genome{$genome}++;
    $plasmid_hit_gene{$genome} .= "$gene, ";

}

## BLAST to IMG complete genomes
open(file, "$out/06/$i.10k.fna.circ.fna.mgm.faa.img.dmd");
while(<file>){
    chomp;
    ($gene, $e) = (split/\t/)[0, 10];
    if($e>0.00001){next}
    $genome = $gene;
    $genome =~s /\_[0-9]{1,3}$//;
    if($img_hit_gene{$genome} =~ /$gene/){next}
    $img_hit_genome{$genome}++;
    $img_hit_gene{$genome} .= "$gene, ";
}

print "Contig\t";
print "Classification\t";
print "Length\t";
print "DeepVifFinder p-value\t";
print "# of phage hallmark genes\t";
print "# of plasmid hallmark genes\t";
print "CheckV provirus\t";
print "CheckV provirus proportion\t";
print "CheckV quality\t";
print "CheckV completeness\t";
print "CheckV contamination\t";
print "Phage hit genes / plasmid hit genes\t";
print "Phage hit genes\t";
print "Plasmid hit genes\t";
print "Bacterial marker genes\t";
print "23S rRNA\t";
print "16S rRNA\t";
print "5S rRNA\n";


foreach $i (sort keys %contig){
    if($mg{$i} =~ /^$/){$mg{$i} = 0}
    if($rrna1{$i} =~ /^$/){$rrna1{$i} = 0}
    if($rrna2{$i} =~ /^$/){$rrna2{$i} = 0}
    if($rrna3{$i} =~ /^$/){$rrna3{$i} = 0}

    if($plasmid_hit_genome{$i} =~ /^$/){$plasmid_hit_genome{$i} = 1}
    if($img_hit_genome{$i} =~ /^$/){$img_hit_genome{$i} = 0}
    $fold = $img_hit_genome{$i}/$plasmid_hit_genome{$i};

    if($length{$i} >= 10000 and  $dvf_p{$i} < 0.05 and $phage_pfam{$i} > 0 and $plasmid_pfam{$i} < 1 and $fold >= 1 and $mg{$i} < 1 and $rrna1{$i} < 1 and $rrna2{$i} < 1 and $rrna3{$i} < 1 and $checkv_pro_pro{$i} < 0.2){
	$classification = "Phage";
    }else{
	$classification = "Others";
    }

    print"$i";
    print"\t$classification";
    print"\t$length{$i}";
    print"\t$dvf_p{$i}";
    print"\t$phage_pfam{$i}";
    print"\t$plasmid_pfam{$i}";
    print"\t$checkv_pro{$i}\t$checkv_pro_pro{$i}\t$checkv_q{$i}\t$checkv_comp{$i}\t$checkv_conta{$i}";
    print"\t$fold\t$img_hit_genome{$i}\t$plasmid_hit_genome{$i}";

    print"\t$mg{$i}";
    print"\t$rrna1{$i}\t$rrna2{$i}\t$rrna3{$i}";
    print"\n";
}
