#!/usr/bin/perl

open(list, "$ARGV[0]");
while(<list>){
    chomp;
    ($contig, $class) = (split/\t/)[0, 1];
    if($class =~ /Phage/){
	$hash{$contig} = 1;
    }
}

open(fna, "$ARGV[1]");
while(<fna>){
    chomp;
    if(/^>/){
	$ok = 0;
	$contig = (split/\t/)[0];
	$contig =~ s/>//;
	$contig =~ s/ .*//;
	if($hash{$contig} == 1){
	    $ok = 1;
	}
    }
    if($ok == 1){
	print "$_\n";
    }
}
