#!/bin/bash

i=$1
out=$2
mkdir $out/02

gmhmmp -m $MGM_PARAM $out/01/$i.10k.fna.circ.fna -f G -A $out/02/$i.10k.fna.circ.fna.mgm.faa -D $out/02/$i.10k.fna.circ.fna.mgm.ffn
gmhmmp -m $MGM_PARAM $out/01/$i.10k.fna.linear.fna -f G -A $out/02/$i.10k.fna.linear.fna.mgm.faa -D $out/02/$i.10k.fna.linear.fna.mgm.ffn

mod_mgm_name.pl $out/02/$i.10k.fna.linear.fna.mgm.faa > $out/02/$i.10k.fna.linear.fna.mgm.faa.temp
mv $out/02/$i.10k.fna.linear.fna.mgm.faa.temp $out/02/$i.10k.fna.linear.fna.mgm.faa
mod_mgm_name.pl $out/02/$i.10k.fna.linear.fna.mgm.ffn > $out/02/$i.10k.fna.linear.fna.mgm.ffn.temp
mv $out/02/$i.10k.fna.linear.fna.mgm.ffn.temp $out/02/$i.10k.fna.linear.fna.mgm.ffn

mod_mgm_name.pl $out/02/$i.10k.fna.circ.fna.mgm.faa > $out/02/$i.10k.fna.circ.fna.mgm.faa.temp
mv $out/02/$i.10k.fna.circ.fna.mgm.faa.temp $out/02/$i.10k.fna.circ.fna.mgm.faa
mod_mgm_name.pl $out/02/$i.10k.fna.circ.fna.mgm.ffn > $out/02/$i.10k.fna.circ.fna.mgm.ffn.temp
mv $out/02/$i.10k.fna.circ.fna.mgm.ffn.temp $out/02/$i.10k.fna.circ.fna.mgm.ffn
#rm -rf $out/02/$i.gff
