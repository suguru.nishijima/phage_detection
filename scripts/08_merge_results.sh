#!/bin/bash

i=$1
out=$2

## summarize all results

merge.all_result.pl $i $out > $out/summary.tsv
extract_phage_sequences.pl $out/summary.tsv $i > $out/phage_sequences.fna
