#!/usr/bin/perl

#perl determine_phage.pl besthit.tsv

$pfamphage="db/phage_pfam.tsv";
$pfamplasmid="db/plasmid_pfam.tsv";

open(file,"$pfamphage");
while(<file>){
    $id=(split/\t/)[0];
    $phage{$id}=1;
}

open(file,"$pfamplasmid");
while(<file>){
    $id=(split/\t/)[0];
    $plasmid{$id}=1;
}

$file = $ARGV[0];
open(file, "$file");
while(<file>){
    chomp;
    $a++;
    if($a==1){next}
    ($name, $id, $prob) = (split/\t/)[0, 5, 8];
    $name =~s /\_[0-9]+?$//;
    $name{$name}=1;
    #print"$id\t$prob\t$phage{$id}\t$plasmid{$id}\t$id\t$_\n";
    if($prob < 90){next}
    if($phage{$id} !~ /^$/){
	$phage{$name} .= "$id, ";
	$phage_gene_num{$name}++;
    }
    if($plasmid{$id} !~ /^$/){
	$plasmid{$name} .= "$id, ";
	$plasmid_gene_num{$name}++;
    }
}
foreach $i (sort keys %name){
    $phage{$i} =~s/\, $//;
    $plasmid{$i} =~s/\, $//;
    if($phage_gene_num{$i} =~ /^$/){$phage_gene_num{$i} = 0}
    if($plasmid_gene_num{$i} =~ /^$/){$plasmid_gene_num{$i} = 0}
    print"$i\t$phage_gene_num{$i}\t$plasmid_gene_num{$i}\t$phage{$i}\t$plasmid{$i}\t$file\n";
}
