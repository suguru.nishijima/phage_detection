#!/bin/bash

i=$1
out=$2

mkdir $out/04

faa=$out/02/$i.10k.fna.circ.fna.mgm.faa
out=$out/04/$i.10k.fna.circ.fna.mgm.faa.hhblits
j_db=db/IMGVR_circular_phage_genes.faa
hmm_db=db/pfam

rm -rf $out/04
pipeline_for_high_sensitive_domain_search -i $faa -o $out -J $j_db -H $hmm_db
rm -rf $out/work/
check.pfam_result.pl $out/result/*/ungrouped/besthit.tsv > $out.sort
mv $out/result/*/ungrouped/besthit.tsv $out.txt
ext.pfam_gene.pl $out.txt
