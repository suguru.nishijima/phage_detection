#!/bin/bash

i=$1
out=$2

mkdir $out/01

## extract >10k bp and circular contigs
ext.10kb.mod_name.pl $i > $i.10k.fna
check_circularity.mod.pl --force $i.10k.fna $i.10k.fna.circ_check
cat $i.10k.fna.circ_check/*cut.fa > $i.10k.fna.circ.fna
for j in `ls $i.10k.fna.circ_check/*cut.fa|cut -f2 -d"/"|cut -f1 -d"."`;do rm $i.10k.fna.circ_check/$j*;done
cat $i.10k.fna.circ_check/*fa > $i.10k.fna.linear.fna
rm -rf $i.10k.fna.circ_check/

mv $i.10k.fna $i.10k.fna.circ.fna $i.10k.fna.linear.fna $out/01
