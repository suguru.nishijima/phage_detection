#!/usr/bin/perl

$pfamphage="db/phage_pfam.tsv";

open(file,"$pfamphage");
while(<file>){
    $id=(split/\t/)[0];
    $phage{$id}=1;
}

$file = $ARGV[0];
open(file, "$file");
while(<file>){
    chomp;
    $a++;
    if($a==1){next}
    ($name, $id, $func, $prob) = (split/\t/)[0, 5, 6, 8];
    #$name =~s /\_[0-9]+?$//;
    $name{$name}=1;
    if($prob < 90){next}
    if($phage{$id} !~ /^$/){
	$phage{$name} = 1;
	$id{$name} .= "${id}_";
	$func{$name} .= "${func}_";
	$phage_gene_num{$name}++;
    }
}

# faa
$file =~ s/.hhblits.*//;
open($write, ">$file.marker.faa");
open(file, "$file");
while(<file>){
    chomp;
    if(/>/){
	$ok = 0;
	$name = $_;
	$name =~ s/>//;
	if($phage{$name} == 1){
	    $ok = 1;
	    $id{$name} =~ s/\_$//;
	    $func{$name} =~ s/\_$//;
	    print $write ">$name, $id{$name}, $func{$name}\n";
	}
	next;
    }
    if($ok == 1){
	print $write "$_\n";
    }
}

# ffn
$file =~ s/faa$/ffn/;
open(file, "$file");
open($write, ">$file.marker.ffn");
while(<file>){
    chomp;
    if(/>/){
	$ok = 0;
	$name = $_;
	$name =~ s/>//;
	if($phage{$name} == 1){
	    $ok = 1;
	    $id{$name} =~ s/\, $//;
	    $func{$name} =~ s/\, $//;
	    print $write ">$name, $id{$name}, $func{$name}\n";
	}
	next;
    }
    if($ok == 1){
	print $write "$_\n";
    }
}
