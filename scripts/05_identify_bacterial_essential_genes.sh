#!/bin/bash

i=$1
out=$2

mkdir $out/05

## fetchMG
fetchMG.pl -m extraction -p -v -o $out/05/$i.10k.fna.circ.fna.mgm.faa.mg $out/02/$i.10k.fna.circ.fna.mgm.faa
cat $out/05/$i.10k.fna.circ.fna.mgm.faa.mg/*faa > $out/05/$i.10k.fna.circ.fna.mgm.faa.mg.faa

## barrnap
barrnap $out/01/$i.10k.fna.circ.fna > $out/05/$i.10k.fna.circ.fna.rrna
